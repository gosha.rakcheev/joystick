#include <Joystick.h>

// Requires Arduino Joystick Library https://github.com/MHeironimus/ArduinoJoystickLibrary
Joystick_ Joystick;
 
int JoystickX;
int JoystickY;

int[5] currentButtonStates = {0,0,0,0,0};
int[5] lastButtonStates = {0,0,0,0,0};

void setup() {
  // switches (cross)
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  // switch (trigger)
  pinMode(8, INPUT_PULLUP);
  // Hall effect sensors
  pinMode(A0, INPUT_PULLUP); 
  pinMode(A1, INPUT_PULLUP);
  
  // Cоздаем джойстик
  Joystick.begin();
  Joystick.setXAxisRange(0, 1024); 
  Joystick.setYAxisRange(0, 1024);  
}

void loop() {
// Read Joystick
  JoystickX = analogRead(A9);
  JoystickY = analogRead(A8);

// Read Switches
  updateButton(0, 4);
  updateButton(1, 5);
  updateButton(2, 6);
  updateButton(3, 7);
  updateButton(4, 8);

// Output Controls
  Joystick.setXAxis(JoystickX);
  Joystick.setYAxis(JoystickY);

  Joystick.sendState();

} 

void updateButton(int buttonNumber, int buttonPin) {
  currentButtonStates[buttonNumber] = digitalRead(buttonPin);
  if (currentButtonStates[buttonNumber] != lastButtonStates[buttonNumber]) {
    Joystick.setButton(buttonNumber, currentButtonStates[buttonNumber]);
    lastButtonStates[buttonNumber] = currentButtonStates[buttonNumber];
  } 
}
