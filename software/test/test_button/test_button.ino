int currentButtonStates[5] = {0,0,0,0,0};
int lastButtonStates[5] = {0,0,0,0,0};

void setup() {
  // switches (cross)
  Serial.begin(9600);
  pinMode(4, INPUT);
}

void loop() {

// Read Switches
  updateButton(0, 4);

} 

void updateButton(int buttonNumber, int buttonPin) {
  currentButtonStates[buttonNumber] = digitalRead(buttonPin);
  if (currentButtonStates[buttonNumber] != lastButtonStates[buttonNumber]) {
    Serial.println(currentButtonStates[buttonNumber]);
    lastButtonStates[buttonNumber] = currentButtonStates[buttonNumber];
  } 
}
