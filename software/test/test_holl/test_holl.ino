#include <Joystick.h>

// Requires Arduino Joystick Library https://github.com/MHeironimus/ArduinoJoystickLibrary
Joystick_ Joystick;
 
int JoystickX;
int JoystickY;

int currentButtonStates[5] = {0,0,0,0,0};
int lastButtonStates[5] = {0,0,0,0,0};

void setup() {
  // Hall effect sensors
  pinMode(A0, INPUT_PULLUP); 
  pinMode(A1, INPUT_PULLUP);
  // Cоздаем джойстик
  Joystick.begin();
  Joystick.setXAxisRange(0, 1024); 
  Joystick.setYAxisRange(0, 1024);  
}

void loop() {
// Read Joystick
  JoystickX = analogRead(A0);
  JoystickY = analogRead(A1);

// Output Controls
  Joystick.setXAxis(JoystickX);
  Joystick.setYAxis(JoystickY);

  Joystick.sendState();
}
